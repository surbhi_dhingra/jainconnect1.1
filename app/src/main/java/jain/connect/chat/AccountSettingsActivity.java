package jain.connect.chat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import jain.connect.chat.common.ActivityBase;
import jain.connect.chat.dialogs.AlcoholViewsSelectDialog;
import jain.connect.chat.dialogs.BloodDonationSelectDialog;
import jain.connect.chat.dialogs.BloodGroupSelectDialog;
import jain.connect.chat.dialogs.DonationSelectDialog;
import jain.connect.chat.dialogs.GenderSelectDialog;
import jain.connect.chat.dialogs.ImportantInOthersSelectDialog;
import jain.connect.chat.dialogs.PersonalPrioritySelectDialog;
import jain.connect.chat.dialogs.PoliticalViewsSelectDialog;
import jain.connect.chat.dialogs.ProfessionSelectDialog;
import jain.connect.chat.dialogs.RelationshipStatusSelectDialog;
import jain.connect.chat.dialogs.SampradayaSelectDialog;
import jain.connect.chat.dialogs.SmokingViewsSelectDialog;
import jain.connect.chat.dialogs.WorldViewSelectDialog;
import jain.connect.chat.dialogs.YouLikeSelectDialog;
import jain.connect.chat.dialogs.YouLookingSelectDialog;

public class AccountSettingsActivity extends ActivityBase implements GenderSelectDialog.AlertPositiveListener, SampradayaSelectDialog.AlertPositiveListener, RelationshipStatusSelectDialog.AlertPositiveListener, BloodGroupSelectDialog.AlertPositiveListener, BloodDonationSelectDialog.AlertPositiveListener, ProfessionSelectDialog.AlertPositiveListener, DonationSelectDialog.AlertPositiveListener, PoliticalViewsSelectDialog.AlertPositiveListener, WorldViewSelectDialog.AlertPositiveListener, PersonalPrioritySelectDialog.AlertPositiveListener, ImportantInOthersSelectDialog.AlertPositiveListener, SmokingViewsSelectDialog.AlertPositiveListener, AlcoholViewsSelectDialog.AlertPositiveListener, YouLookingSelectDialog.AlertPositiveListener, YouLikeSelectDialog.AlertPositiveListener {

    Toolbar mToolbar;

    Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(jain.connect.chat.R.layout.activity_account_settings);

        mToolbar = (Toolbar) findViewById(jain.connect.chat.R.id.toolbar);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        if (savedInstanceState != null) {

            fragment = getSupportFragmentManager().getFragment(savedInstanceState, "currentFragment");

        } else {

            fragment = new AccountSettingsFragment();
        }

        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(jain.connect.chat.R.id.container_body, fragment).commit();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {

        super.onSaveInstanceState(outState);

        getSupportFragmentManager().putFragment(outState, "currentFragment", fragment);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        fragment.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(jain.connect.chat.R.menu.menu_account_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.


        switch (item.getItemId()) {

            case android.R.id.home: {

                finish();
                return true;
            }

            default: {

                return super.onOptionsItemSelected(item);
            }
        }
    }

    @Override
    public void onGenderSelect(int position) {

        AccountSettingsFragment p = (AccountSettingsFragment) fragment;
        p.getGender(position);
    }

    @Override
    public void onRelationshipStatusSelect(int position) {

        AccountSettingsFragment p = (AccountSettingsFragment) fragment;
        p.getRelationshipStatus(position);
    }

    public void onBloodDonationSelect(int position) {

        AccountSettingsFragment p = (AccountSettingsFragment) fragment;
        p.getBloodDonation(position);
    }

    public void onBloodGroupSelect(int position) {

        AccountSettingsFragment p = (AccountSettingsFragment) fragment;
        p.getBloodGroup(position);
    }

    public void onProfessionSelect(int position) {

        AccountSettingsFragment p = (AccountSettingsFragment) fragment;
        p.getProfession(position);
    }

    public void onDonationSelect(int position) {

        AccountSettingsFragment p = (AccountSettingsFragment) fragment;
        p.getDonation(position);
    }

    public void onSampradayaSelect(int position) {

        AccountSettingsFragment p = (AccountSettingsFragment) fragment;
        p.getSampradaya(position);
    }

    @Override
    public void onPoliticalViewsSelect(int position) {

        AccountSettingsFragment p = (AccountSettingsFragment) fragment;
        p.getPoliticalViews(position);
    }

    @Override
    public void onWorldViewSelect(int position) {

        AccountSettingsFragment p = (AccountSettingsFragment) fragment;
        p.getWorldView(position);
    }

    @Override
    public void onPersonalPrioritySelect(int position) {

        AccountSettingsFragment p = (AccountSettingsFragment) fragment;
        p.getPersonalPriority(position);
    }

    @Override
    public void onImportantInOthersSelect(int position) {

        AccountSettingsFragment p = (AccountSettingsFragment) fragment;
        p.getImportantInOthers(position);
    }

    @Override
    public void onSmokingViewsSelect(int position) {

        AccountSettingsFragment p = (AccountSettingsFragment) fragment;
        p.getSmokingViews(position);
    }

    @Override
    public void onAlcoholViewsSelect(int position) {

        AccountSettingsFragment p = (AccountSettingsFragment) fragment;
        p.getAlcoholViews(position);
    }

    @Override
    public void onYouLookingSelect(int position) {

        AccountSettingsFragment p = (AccountSettingsFragment) fragment;
        p.getYouLooking(position);
    }

    @Override
    public void onYouLikeSelect(int position) {

        AccountSettingsFragment p = (AccountSettingsFragment) fragment;
        p.getYouLike(position);
    }
}
